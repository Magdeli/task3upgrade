﻿using System;

namespace Task8
{
    class Program
    {
        static void Main(string[] args)
        {
            #region \\Variables
            // The variable is declared.

            int n;

            #endregion

            #region \\Getting the size of the square
            // Input is gathered on how large the square should be.

            do
            {
                Console.WriteLine("Please enter a number for how large you want the square:");

                n = int.Parse(Console.ReadLine());

            } while (n % 1 != 0);

            #endregion

            #region \\Making the square
            // The square is made using the method makeSquare

            makeSquare(n);

            #endregion
        }

        public static void makeSquare(int n)
        {
            // Variables are declared
            string square;
            string space;

            // The square is made and printed out as text
            for (int i = 0; i < n; i++)
            {
                if (i == 0 || i == (n - 1))
                {
                    square = new String('#', n);
                    Console.WriteLine(square);
                }
                else
                {
                    square = "#";
                    space = new string(' ', (n - 2));
                    Console.WriteLine(square + space + square);
                }
            }
        }
    }
}
